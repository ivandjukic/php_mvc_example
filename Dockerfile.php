FROM php:7.1.8-apache

COPY ./code/ /var/www/app/
COPY .docker/vhost.conf /etc/apache2/sites-available/000-default.conf

RUN chown -R www-data:www-data /var/www/app \
&& a2enmod rewrite

RUN docker-php-ext-install mbstring pdo pdo_mysql

