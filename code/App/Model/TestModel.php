<?php

namespace App\Model;

use Core\Model\ModelBase as ModelBase;

class ProductModel extends ModelBase
{
    protected $table_name = 'products';
}