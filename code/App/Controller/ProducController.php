<?php

namespace App\Controller;

use Core\Controller\ControllerBase as Controller;
use App\Model\ProductModel as ProductModel;


class ProductCOntroller extends Controller
{
    function show(){
        $product_model = new ProductModel();
        $products = $product_model->all();
        $this->view('index', ['products' => $products]);
    }
}