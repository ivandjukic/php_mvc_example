<?php

namespace Core\Router;

use App\Routes as Routes;

class Router
{
    
    protected $routes = [];
   
    function __construct() 
    {
        $this->routes = Routes::$available_routes;
    }

    private function urlExist($url)
    {
        foreach ($this->routes as $route) {

            if($route['route'] == $url) {
                return true;
            }
        }
        return false;
    }
    
    private function getUrlAction($url) 
    {
        foreach ($this->routes as $route) {
            if($route['route'] == $url) {
                return $route;
            }
        }
        return null;
    }
    
    public function handleRequest($request)
    {
        $request_method = $this->getRequestMethod($request);
        $request_resource = $this->getRequestResource($request);
        $request_parameters = $this->getRequestQueryParameters($request);

        if ($this->urlExist($request_resource)) {

            $action = $this->getUrlAction($request_resource);
            $controller = $this->getController($action['controller']);
            if (class_exists($controller)) {
                $method = $action['method'];
                if(method_exists($controller, $method)) {
                    $class_instance = new $controller;
                    $class_instance->$method();
                }else {
                    throw new \Exception("Function $method not exist in controller $controller");
                }
            } else {
                throw new \Exception("Controller class $controller not found");
            }
        } else {
            throw new \Exception("Action $request_resource not exist");
        }
    }

    private function getRequestMethod($request) 
    {
        return $request['REQUEST_METHOD'];
    } 
    private function getRequestResource($request) 
    {
        return $request['PATH_INFO']? $request['PATH_INFO'] : [];
    } 
    private function getRequestQueryParameters($request) 
    {
        return isset($request['QUERY_STRING'])? $request['QUERY_STRING'] : [];
    } 

    protected function getController($controller_name)
    {
        return "App\Controller\\$controller_name";
    }
}